import Example from './components/Example';
import InputIdCard from './components/InputIdCard';
import antdCss from 'antd/dist/antd.css';
import lightTheme from './css/light-theme.css';
export { antdCss, lightTheme, InputIdCard, Example };
