/// <reference types="react" />
interface Props {
    placeholder: string;
}
export default function InputIdCard({ placeholder }: Props): JSX.Element;
export {};
