/// <reference types="react" />
interface Props {
    text: string;
}
export default function Example({ text }: Props): JSX.Element;
export {};
